window.onload = function main() {

    var options = document.getElementById("clearmenu").options;
    options[0].selected = true;
    options[1].selected = false;
    options[2].selected = false;
    options[3].selected = false;

    var options = document.getElementById("pointmenu").options;
    options[0].selected = false;
    options[1].selected = true;
    options[2].selected = false;
    options[3].selected = false;

    var canvas = document.getElementById("webgl");
    // Initialize the GL context
    gl = WebGLUtils.setupWebGL(canvas);

    // Only continue if WebGL is available and working
    if (!gl) {
        alert("Unable to initialize WebGL. Your browser or machine may not support it.");
        return;
    }

    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    gl.viewport(0, 0, canvas.width, canvas.height);
    // Set clear color to blue, fully opaque
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    // Clear the color buffer with specified clear color


    var index = 0;
    var drawmode = "point";
    var trianglevert = 0;
    var indexArrayPoints = [];
    var indexArrayTri = [];
    var max_verts = 1000;
    var vertices = [];
    var triangles = [];
    var initcolors = [vec4(0.0, 0.0, 0.0, 1.0)];
    //Write the list of all the possible colors
    var colors = [
        vec4(0.3921, 0.5843, 0.9294, 1.0),
        vec4(0.0, 0.0, 0.0, 1.0),
        vec4(1.0, 1.0, 1.0, 1.0),
        vec4(1.0, 0.0, 0.0, 1.0)
    ];

    canvas.addEventListener("click", function(ev) {
        var cpointMenu = document.getElementById("pointmenu");
        var bgcolor = colors[cpointMenu.selectedIndex];
        var color = vec4(bgcolor[0], bgcolor[1], bgcolor[2], bgcolor[3]);
        //correction for the coordinates of the points
        var correction = event.target.getBoundingClientRect();
        //define the exactly coordinates
        var t = vec2(-1 + 2 * (event.clientX - correction.left) / canvas.width, -1 + 2 * (canvas.height - event.clientY + correction.top) / canvas.height);

        if (drawmode == "point") {

            //add coordinates to the vector
            vertices.push(t);
            //push the vector to the buffer
            gl.bindBuffer(gl.ARRAY_BUFFER, bufferId); //ok now the buffer is used for vertices
            gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec2'] * index, flatten(t));
            gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
            gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec4'] * index, flatten(color));
            indexArrayPoints.push(index);
            index++;

        } else if (drawmode == "triangle") {
            triangles.push(t);
            //push the vector to the buffer
            gl.bindBuffer(gl.ARRAY_BUFFER, bufferId); //ok now the buffer is used for vertices
            gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec2'] * index, flatten(t));
            gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId);
            gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec4'] * index, flatten(color));
            if (trianglevert == 2) {
                indexArrayPoints.pop();
                indexArrayTri.push(indexArrayPoints[indexArrayPoints.length - 1]);
                indexArrayPoints.pop();
                trianglevert = 0;
            } else {
                //add coordinates to the vector
                indexArrayPoints.push(index);
                trianglevert++;
            }
            index++;
        }

    });

    //create buffer colors
    var cBufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cBufferId); //ok now the buffer is used for colors
    gl.bufferData(gl.ARRAY_BUFFER, max_verts * sizeof['vec4'], gl.STATIC_DRAW);
    // Associate out shader variables with our data buffer
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(initcolors));
    gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec4'] * 1, flatten(initcolors));
    gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec4'] * 2, flatten(initcolors));
    var vColor = gl.getAttribLocation(program, "vColor");
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);


    //create buffer vertices
    var bufferId = gl.createBuffer();
    //initialize buffer
    gl.bindBuffer(gl.ARRAY_BUFFER, bufferId); //ok now the buffer is used for vertices
    gl.bufferData(gl.ARRAY_BUFFER, max_verts * sizeof['vec2'], gl.STATIC_DRAW);
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(vertices));
    // Associate out shader variables with our data buffer
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);


    render();


    function render() {
        gl.clear(gl.COLOR_BUFFER_BIT);
        for (var i = 0; i < index; i++) {
            if (indexArrayPoints.includes(i)) {
                gl.drawArrays(gl.POINTS, i, 1); //GLenum mode, GLint first, GLsizei count
            } else if (indexArrayTri.includes(i)) {
                gl.drawArrays(gl.TRIANGLES, i, 3);
            }
        }
        //render it
        window.requestAnimFrame(render);
    }

    var pointbutton = document.getElementById("pointbutton");
    pointbutton.addEventListener("click", function(event) {
        drawmode = "point";
        trianglevert = 0;
    });

    var trianglebutton = document.getElementById("trianglebutton");
    trianglebutton.addEventListener("click", function(event) {
        drawmode = "triangle";
        trianglevert = 0;
    });


    var clearMenu = document.getElementById("clearmenu");
    var clearButton = document.getElementById("clearbutton");

    clearButton.addEventListener("click", function(event) {

        var bgcolor = colors[clearMenu.selectedIndex];
        gl.clearColor(bgcolor[0], bgcolor[1], bgcolor[2], bgcolor[3]);

        index = 0;
        vertices = [];
        triangles = [];
        trianglevert = 0;
        indexArrayPoints = [];
        indexArrayTri = [];

    });
}