window.onload = function main() {
    var canvas = document.getElementById("webgl");
    // Initialize the GL context
    gl = WebGLUtils.setupWebGL(canvas);

    // Only continue if WebGL is available and working
    if (!gl) {
        alert("Unable to initialize WebGL. Your browser or machine may not support it.");
        return;
    }

    vertices = [
        vec2(1, 0),
        vec2(0, 0),
        vec2(1, 1)
    ];


    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    gl.viewport(0, 0, canvas.width, canvas.height);
    // Set clear color to black, fully opaque
    gl.clearColor(0.3921, 0.5843, 0.9294, 1.0);
    // Clear the color buffer with specified clear color

    var max_verts = 1000;
    var index = 3;
    var numPoints = 0;
    var vBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vBuffer);

    gl.bufferData(gl.ARRAY_BUFFER, max_verts * sizeof['vec2'], gl.STATIC_DRAW);
    gl.bufferSubData(gl.ARRAY_BUFFER, 0, flatten(vertices));



    var canvas = document.getElementById("webgl");
    canvas.addEventListener("click", function(ev) {
        //correction for the coordinates of the points
        var correction = event.target.getBoundingClientRect();
        //define the exactly coordinates
        var t = vec2(-1 + 2 * (event.clientX - correction.left) / canvas.width, -1 + 2 * (canvas.height - event.clientY + correction.top) / canvas.height);
        //add coordinates to buffer
        gl.bufferSubData(gl.ARRAY_BUFFER, sizeof['vec2'] * index, flatten(t));
        numPoints = Math.max(numPoints, ++index);
        index %= max_verts;

    })



    // Associate out shader variables with our data buffer

    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    render();


    function render() {
        gl.clear(gl.COLOR_BUFFER_BIT);
        gl.drawArrays(gl.POINTS, 0, index);
        window.requestAnimationFrame(render, canvas);
    }

}